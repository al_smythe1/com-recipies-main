drop database if exists recipemain;
create database recipemain;
use recipemain;
drop table if exists recipes;
CREATE TABLE recipes(
    recipe_id int NOT NULL,
    recipe_name varchar(800),
    recipe_instructions VARCHAR(2400),
    recipe_user VARCHAR(250),
    PRIMARY KEY(recipe_id)
);
drop table if exists ingredients;
CREATE TABLE ingredients(
    ingredients_id int NOT NULL,
    ingredients_name varchar(800),
    PRIMARY KEY(ingredients_id)
);
drop table if exists ring;
CREATE TABLE ring(
    recipe_id int NOT NULL,
    ingredients_id int NOT NULL,
    PRIMARY KEY(recipe_id, ingredients_id),
    FOREIGN KEY(recipe_id) REFERENCES recipes(recipe_id),
    FOREIGN KEY(ingredients_id) REFERENCES ingredients(ingredients_id)
);

INSERT INTO recipes VALUES(
    0,
    'Spag Bol',
    'add onions-ready made pasta sauce-choped vegtables-pasta',
    'ADMIN'
),(
    1,
    'Cheese Toastie',
    'two slices bread-grated cheddar-babybell on oppsite sides of the bread',
    'ADMIN'

),(
    2,
    'Tuna Sandwich',
    'add onions-2 slices of bread-tuna',
    'ADMIN'
);

INSERT INTO ingredients VALUES(
    0, 'Onion'),
    (1,'Garlic'),
    (2,'Chilli powder'),
    (3,'Paprika'),
    (4,'minced beef'),
    (5,'pasta'),
    (6,'bread'),
    (7,'tuna'),
    (8,'vegtables'),
    (9,'babybell'),
    (10,'cheddar cheese'),
    (11,'pasta sauce'),
    (12,'Green Peppers'),
    (13,'Chicken'),
    (14,'Sausages'),
    (15,'eggs'),
    (16,'pepper'),
    (17,'oregano'),
    (18,'mixed herbs'),
    (19,'oil'),
    (20,'butter'
);

INSERT INTO ring VALUES(
   0,0),
   (0,11),
   (0,8),
   (0,5), 

  (1,6),
  (1,20),
  (1,10),
  (1,9),

  (2,0),
  (2,7),
  (2,6

);
import mysql.connector
from config import db_config

def get_food():
    recipes2 = []
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM recipes")
    recipes = cursor.fetchall()
    for recipe in recipes:
        recipes2.append({
            'recipe_id': recipe[0],
            'recipe_name': recipe[1],
            'recipe_instructions': recipe[2],
            'recipe_user': recipe[3]
        })
    return recipes2

def get_info(recipeid):
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM recipes WHERE recipe_id = " + recipeid)
    recipes = cursor.fetchall()    
    return_info = {
        'recipe_name': recipes[0][1],
        'recipe_instructions': recipes[0][2],
        'recipe_user': recipes[0][3]
    }
    return return_info

def get_ingredients(recipeid):
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM ring WHERE recipe_id = " + str(recipeid))
    recipeslink = cursor.fetchall()
    ingredients_list = [] 
    unformatted_ingredient_list = []
    for recipe in recipeslink:
        cursor.execute("SELECT * FROM ingredients WHERE ingredients_id =" + str(recipe[1]))
        unformatted_ingredient_list.append(cursor.fetchall())
    for item in unformatted_ingredient_list:
        ingredients_list.append(item[0][1])
    return ingredients_list
        
def get_recipeamount():
    recipe_list = []
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT recipe_id FROM recipes")
    recipes = cursor.fetchall()
    return len(recipes)

def get_ingredientsidea():
    recipe_ing = []
    unformatted_ingredient_list = []
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT ingredients_name FROM ingredients")
    ingredients = cursor.fetchall()
    for item in ingredients:
        recipe_ing.append(item[0])
    return recipe_ing

def add_recipe(recipeinfo):
    recipeinfo = recipeinfo.split("+")
    newrecipe_id = get_recipeamount()
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    print(recipeinfo)
    cursor.execute("INSERT INTO recipes VALUES(%s, %s, %s, %s)", (str(newrecipe_id), str(recipeinfo[0]), str(recipeinfo[1]), str(recipeinfo[2]),))
    database.commit()
    ingredients = recipeinfo[3].split(", ")
    ingredient_id = []
    for item in ingredients:
        cursor.execute("SELECT ingredients_id FROM ingredients WHERE ingredients_name = %s", (str(item),))
        id = cursor.fetchall()
        for i in id:
            i = str(i[0])
            ingredient_id.append(i)
    for item in ingredient_id:
        cursor.execute("INSERT INTO ring VALUES(%s, %s)", (str(newrecipe_id), str(item),))
        database.commit()
from flask import Flask,  render_template, request, url_for, redirect
from util import get_food,  get_info, get_ingredients,get_ingredientsidea,add_recipe


#from .models import User 
#from werkzeug.security import generate_password_hash,check_password_hash
app = Flask(__name__)
app.secret_key = "1234"
#user accounts

#main route
@app.route('/')  #homePage
def home():
    return render_template('home.html')

@app.route('/about')  #homePage
def about():
    return render_template('about.html')

@app.route('/recipes')  #homePage
def recipes():
    return render_template('recipes.html', recipelist = get_food())

@app.route('/recipes/<recipe_id>')
def recipeinfo(recipe_id): 
    return render_template('info.html', recipeinfo = get_info(recipe_id), ingredients = get_ingredients(recipe_id))

@app.route('/add')
def add(): 
    return render_template('add.html', ingredients = get_ingredientsidea())

@app.route('/add/newrecipe/<recipeinfo>')
def newrecipe(recipeinfo):
    print(recipeinfo)
    add_recipe(recipeinfo)
    return redirect('/')
    

#this will stay below all my app.route
if __name__ == '__main__':
    app.run( host="0.0.0.0", debug=True)
    